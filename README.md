<a href="https://www.youtube.com/DebianBrasilOficial"><img width="480px" src="assista.png"/></a>

# #FiqueEmCasaUseDebian

Este evento é fortemente inspirado em outro evento chamado
[FiqueEmCasaConf](https://github.com/linuxtips/FiqueEmCasaConf) organizado pelo
Jeferson Fernando do [LINUXtips](https://www.linuxtips.io/).

## Quando?
~~3 a 30 de maio de 2020.~~

3 de maio a 6 de junho de 2020.

### MiniDebConf mundial online
Nos dias 30 e 31 de maio acontecerá a MiniDebConf mundial online.

Veja mais informações em: <https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline>

## Onde?
Canal no Youtube do Debian Brasil: [youtube.com/DebianBrasilOficial](https://www.youtube.com/DebianBrasilOficial)
- Somente a maratona de empacotamento aos sábados não será pelo Youtube.

## Será ao vivo?
Sim.

## Horário?
De segunda a sexta-feira as atividades começarão às 20:00h (Horário de Brasília), com
duração máxima de 2 horas.

Nos sábados/domingos as atividades começarão às 14:00h e terão duração de 4 horas.

A palestra do Jonathan Carter no dia 4 será às 18h porque ele mora na África do Sul e
tivemos que antecipar o início pra não ficar muito tarde pra ele.

## Qual o formato das atividades?
Serão palestras ou atividades mão na massa relacionadas ao [Projeto
Debian](https://www.debian.org).

## Código de conduta

Iremos seguir o [Código de Conduta
Debian](https://www.debian.org/code_of_conduct) e o [Código de Conduta da
DebConf](https://www.debconf.org/codeofconduct.shtml) 


## Calendário de palestras confirmadas

 | dom | seg | ter | qua | qui | sex | sab 
 :---:|:---:|:---:|:---:|:---:|:---:|:---:
03 <br>20h00<br>[Abertura](https://www.youtube.com/watch?v=cjc7HQGHGuE)<br>**Organização**|04 <br>**18h00**<br>[A mixed bag of Debian](https://www.youtube.com/watch?v=Zu6ig8lIw3k)<br>**Jonathan Carter**|05<br>20h00<br>[Debian - O sistema Universal!](https://youtu.be/K_BIO7c60oI)<br>**Daniel Lenharo**|06<br>20h00<br>[Empacotamento de software no Debian - Uma rápida introdução](https://youtu.be/EwlR5ZSanng)<br>**Eriberto Mota**|07<br>20h00<br>[Customizando a instalação do Debian com preseed](https://www.youtube.com/watch?v=BpkYoSgiLag)<br>**Blau Araujo**|08<br>20h00<br>[O Projeto Debian quer você!](https://www.youtube.com/watch?v=xSJAkT1oR1Y)<br>**Paulo Santana**|09<br>14h00<br>Maratona de empacotamento Debian para iniciantes<br>**Eriberto, Giovani, Kanashiro, Márcio, Thiago, outros DDs**|
10<br>reservado<br>|11<br>20h00<br>[Empacotamento Debian com Docker](https://www.youtube.com/watch?v=G3zdpXAlYq0)<br>**Daniel Pimentel**|12<br>20h00<br>[Desvendando Wi-Fi](https://youtu.be/Lc9GvmBM2No)<br>**Paulo kretcheu**|13<br>20h00<br>[Não quebre o Debian](https://youtu.be/yvgQL_cV3fo)<br>**Leandro Ramos**|14<br>20h00<br>[GnuPG - Assinatura de Chaves](https://youtu.be/LgwuZtp3nGM)<br>**Thiago Andrade**|15<br>20h00<br>[Telefonia Open source com Debian](https://youtu.be/HTtBTG82ycI)<br>**Leonardo Rodigues**|16<br>14h00<br>Maratona de empacotamento Debian para iniciantes<br>**Eriberto, Giovani, Kanashiro, Márcio, Thiago, outros DDs**|
17<br>reservado<br>|18<br>20h00<br>[Tratando bugs no Debian ao vivo](https://youtu.be/Ibax4tP9nNM)<br>**Antonio Terceiro**<br>|19<br>20h00<br>[Um passeio virtual pelas páginas do Debian](https://youtu.be/gP7akib7jlQ)<br>**Eriberto Mota**|20<br>20h00<br>[Hardening de SSH - Mitigando os riscos do mal uso](https://youtu.be/vA0WfFKwa6U)<br>**Marcio Souza**<br>|21<br>20h00<br>[Como utilizar o GRUB para iniciar Live ISOs](https://youtu.be/m7e1yMd0oKM)<br>**Fernão Vellozo**|22<br>20h00<br>[Não sou programador, como posso ajudar o projeto Debian?](https://youtu.be/1zXEW6OtrCY)<br>**Luiz Eduardo Guaraldo**|23<br>14h00<br>Maratona de empacotamento Debian para iniciantes<br>**Eriberto, Giovani, Kanashiro, Márcio, Thiago, outros DDs**|
24<br>reservado<br>|25<br>20h00<br>[Ubuntu: um fork do Debian?](https://youtu.be/4g0_Da--CIA)<br><br>**Lucas Kanashiro**|26<br>20h00<br>[De noviço a tradutor: uma caminhada com Debian](https://youtu.be/75Ouhkm31d0)<br>**Thiago Nunes**|27<br>20h00<br>[Porque o Debian é o sistema mais inseguro hoje](https://youtu.be/U5FUjZh8nZk)<br>**Michelle Ribeiro e Gleydson Mazioli da Silva**|28<br>20h00<br>[Colabore com o Projeto Debian e impulsione sua carreira](https://youtu.be/gl4t_WLwJXg)<br>**Giovani Ferreira**|29<br>20h00<br>[SysAdmin apps](https://youtu.be/Wz2IAg6MMlU)<br>**Marcelo Gondim**|30<br>14h00<br>Maratona de empacotamento Debian para iniciantes<br>**Eriberto, Giovani, Kanashiro, Márcio, Thiago, outros DDs**|
31<br>reservado<br>|01<br>20h00<br>[A minha primeira experiência com empacotamento e tradução no Debian](https://youtu.be/i0XoAzL1KHI)<br><br>**Eriberto Mota e vários**|02<br>20h00<br>[Debian no pendrive](https://youtu.be/qC7kaKTJqXs)<br>**Paulo kretcheu**|03<br>20h00<br>[CGNAT com NFTables](https://youtu.be/5uOFtkplDts)<br>**Marcelo Gondim**|04<br>20h00<br>[Porque sou usuário do GNU/Linux/Debian](https://youtu.be/_rIaV1ab4CY)<br>**Antonio Marques**|05<br>20h00<br>[Sou programador{a,}, como posso ajudar o Debian?](https://youtu.be/kanDL540lVc)<br>**Antonio Terceiro**|06<br>18h00<br>Vários<br>[**Encerramento**](https://youtu.be/LZsiULEvpFw)|

## Mais detalhes das palestras

Data | Palestrante | Assunto
--- | --- | ---
*03/05/2020* | **Organização** | *Abertura*
*04/05/2020* | **Jonathan Carter** | *A mixed bag of Debian*
*05/05/2020* | **Daniel Lenharo** | *Debian - O Sistema Universal!* - Palestra voltada a fazer uma apresentação inicial do projeto Debian, como a distribuição é organizada e breve introdução nas áreas de contribuição!
*06/05/2020* | **Eriberto** | *Empacotamento de software no Debian - Uma rápida introdução* - Palestra voltada para quem irá realizar a atividade de empacotamento para iniciantes nos sábados e também para curiosos sobre o processo.
*07/05/2020* | **Blau Araujo** | *Customizando a instalação do Debian com preseed*
*08/05/2020* | **Paulo Santana** | *O Projeto Debian quer você!*
*09/05/2020* | **Debian Brasil** | *Empacotamento, das 14:00h às 18h - Instruções [http://eriberto.pro.br/FiqueEmCasaUseDebian/](http://eriberto.pro.br/FiqueEmCasaUseDebian/)*
*10/05/2020* | **reservado** | *reservado*
*11/05/2020* | **Daniel Pimentel** | *Empacotamento Debian com Docker*
*12/05/2020* | **Paulo kretcheu** | *Desvendando Wi-Fi*
*13/05/2020* | **Leandro Ramos** | *Não quebre o Debian*
*14/05/2020* | **Thiago Andrade** | *GnuPG - Assinatura de Chaves* - Será demonstrado a criação de chaves GPG, assinatura de outras chaves públicas (método tradicional e utilização do caff) e o envio para servidores de chaves na Internet e para o Debian.
*15/05/2020* | **Leonardo Rodrigues** |  *Telefonia Open source com Debian* - O objetivo desta palestra é demonstrar a versatilidade e os benefícios da telefonia open source em conjunto com a distribuição Gnu Linux Debian.
*16/05/2020* | **Debian Brasil** | *Empacotamento, das 14:00h às 18h - Instruções [http://eriberto.pro.br/FiqueEmCasaUseDebian/](http://eriberto.pro.br/FiqueEmCasaUseDebian/)*
*17/05/2020* | **reservado** | *reservado*
*18/05/2020* | **Antonio Terceiro** | *Tratando bugs no Debian ao vivo*. Será feita uma breve introdução ao sistema de bugs do Debian (BTS - Bug Tracking system), e em seguida serão tratados alguns bugs ao vivo.
*19/05/2020* | **Eriberto Mota** | *Um passeio virtual pelas páginas do Debian* - Serão mostradas várias páginas interessantes, com muitos recursos, que nem sempre quem precisa conhece. Trata-se um tour virtual nos domínios debian.org e debian.net.
*20/05/2020* | **Marcio Souza** | *Hardening de SSH - Mitigando os riscos do mal uso* - O protocolo SSH é unanimidade como solução de acesso remoto seguro, talvez por isso muitas organizações não implementem nenhuma política de uso, o que pode trazer sérios riscos a segurança de rede. Nós vamos discutir e demonstrar alguns riscos, além é claro de algumas soluções.
*21/05/2020* | **Fernão Vellozo** | *Como utilizar o GRUB para iniciar Live ISOs*
*22/05/2020* | **Luiz Eduardo Guaraldo** | *Não sou programador, como posso ajudar o projeto Debian?*
*23/05/2020* | **Debian Brasil** | *Empacotamento, das 14:00h às 18h - Instruções [http://eriberto.pro.br/FiqueEmCasaUseDebian/](http://eriberto.pro.br/FiqueEmCasaUseDebian/)*
*24/05/2020* | **reservado** | *reservado*
*25/05/2020* | **Lucas Kanashiro** | *Ubuntu: um fork do Debian?*
*26/05/2020* | **Thiago Nunes** | *De noviço a tradutor: uma caminhada com Debian*
*27/05/2020* | **Michelle Ribeiro e Gleydson Mazioli da Silva** | *Porque o Debian é o sistema mais inseguro hoje*
*28/05/2020* | **Giovani Ferreira** | *Colabore com o Projeto Debian e impulsione sua carreira* - Conheça as vantagens ao se envolver com a comunidade Debian e como isto pode influenciar no seu futuro profissional.
*29/05/2020* | **Marcelo Gondim** | *Sysadmin Apps, ferramentas úteis para sysadmins*
*30/05/2020* | **Debian Brasil** | *Empacotamento, das 14:00h às 18h - Instruções [http://eriberto.pro.br/FiqueEmCasaUseDebian/](http://eriberto.pro.br/FiqueEmCasaUseDebian/)*
*31/05/2020* | **reservado** | *reservado*
*01/06/2020* | **Eriberto Mota e vários** | *A minha primeira experiência com empacotamento e tradução no Debian* Venha nos contar ao vivo como foram as maratonas para você!
*02/06/2020* | **Paulo kretcheu** | *Debian no pendrive*
*03/06/2020* | **Marcelo Gondim** | *CGNAT com NFTables*
*04/06/2020* | **Antonio Marques** | *Porque sou usuário do GNU/Linux/Debian*
*05/06/2020* | **Antonio Terceiro** | *Sou programador{a,}, como posso ajudar o Debian?* - se você gosta de programar, existem diversos serviços e ferramentas do Debian com os quais você pode contribuir.
*06/06/2020* | **Vários** | *18h00 - Encerramento*





## Arquivos usados nas apresentações

Data | Arquivo
--- | --- 
*04/05/2020* | [A mixed bag of Debian](arquivos/2020-05-04-a-mixed-bag-of-debian.pdf)
*05/05/2020* | [Debian - o Sistema Universal](arquivos/2020-05-05-Debian-o-sistema-universal.pdf)
*06/05/2020* | [Empacotamento de software no Debian - Uma rápida introdução](http://eriberto.pro.br/palestras/empacotamento_debian.pdf)
*07/05/2020* | [Customizando a instalação do Debian com preseed](arquivos/2020-05-07-customizando-a-instalacao-do-debian-com-preseed.pdf)
*08/05/2020* | [O Projeto Debian quer você!](https://gitlab.com/phls/arquivos-de-apresentacoes/-/blob/master/2020-05-08-FiqueEmCasaUseDebian-o-projeto-debian-quer-voce.pdf)
*11/05/2020* | [Empacotamento Debian com Docker](arquivos/2020-05-11-empacotamento-debian-com-docker.html)
*12/05/2020* | [Desvendando o Wi-Fi](arquivos/2020-05-12-desvendando-o-wifi.pdf)
*13/05/2020* | [Não quebre o Debian](arquivos/2020-05-13-nao-quebre-o-debian.pdf)
*14/05/2020* | [GnuPG - Assinatura de chaves](https://andrade.wiki.br/wp-content/uploads/2020/05/gnupg.pdf)
*15/05/2020* | [Telefonia Open Source com Debian](arquivos/2020-05-15-telefonia-open-source-com-debian.pdf)
*18/05/2020* | [Tratando bugs no Debian ao vivo](arquivos/2020-05-18-tratando-bugs-no-debian.pdf)
*19/05/2020* | [Um passeio virtual pelas páginas do Debian](arquivos/2020-05-19-passeio-virtual-debian.pdf)
*20/05/2020* | [Hardening de SSH - Mitigando os riscos do mal uso](arquivos/2020-05-20-hardening-de-ssh.pdf)
*21/05/2020* | [Como utilizar o GRUB para iniciar Live ISOs](arquivos/2020-05-21-como-utilizar-o-grub-para-iniciar-live-isos.pdf)
*25/05/2020* | [Ubuntu: um fork do Debian?](arquivos/2020-05-25-Ubuntu-um-fork-do-Debian.pdf)
*26/05/2020* | [De noviço a tradutor: uma caminhada com Debian](arquivos/2020-05-26-de-novico-a-tradutor-uma-caminhada-com-debian.pdf)
*27/05/2020* | [Porque o Debian é o sistema mais inseguro hoje](https://speakerdeck.com/michelleribeiro/debian-a-distribuicao-mais-insegura-da-atualidade)
*28/05/2020* | [Colabore com o Projeto Debian e impulsione sua carreira](arquivos/2020-05-28-colabore-com-o-projeto-debian-e-impulsione-sua-carreira.pdf)
*29/05/2020* | [Sysadmin Apps, ferramentas úteis para sysadmins](arquivos/2020-05-29-sysadmin-apps-ferramentas-uteis-para-sysadmins.pdf)
*02/06/2020* | [Debian no pendrive](arquivos/2020-06-02-debian-no-pendrive.pdf)
*03/06/2020* | [CGNAT com NFTables](arquivos/2020-06-03-cgnat-com-nftables.pdf)
*04/06/2020* | [Porque sou usuário do GNU/Linux/Debian](arquivos/2020-06-04-porque-sou-usuario-do-gnu-linux-debian.pdf)
*05/06/2020* | [Sou programador{a,}, como posso ajudar o Debian?](arquivos/2020-06-05-sou-programador-programadora-como-posso-ajudar-o-debian.pdf)
*06/06/2020* | [Encerramento](arquivos/2020-06-06-Encerramento.pdf)

