arquivos = $(patsubst %, public/%, $(wildcard arquivos/*))
all: public/index.html public/assista.png public/style.css $(arquivos)

public/index.html public/assista.png: public

public:
	mkdir -p public

public/index.html: README.md FOOTER.html style.css
	pandoc \
		--metadata pagetitle='#FiqueEmCasaUseDebian' \
		--standalone \
		--from=markdown \
		--to=html \
		--css=style.css \
		--include-after-body=FOOTER.html \
		--output=$@ \
		README.md


public/assista.png: assista.png
	cp $< $@

public/style.css: style.css
	cp $< $@

public/arquivos:
	mkdir -p $@

$(arquivos): public/arquivos

$(arquivos): public/arquivos/%: arquivos/%
	cp $< $@

clean:
	$(RM) -r public
